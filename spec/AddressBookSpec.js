describe('AddressBook', function () {
	var _AddressBook, this_Contact;

	beforeEach(function (){
		_AddressBook  = new AddressBook();
		 this_Contact = new Contact();
	});

	
	it('should be able to add a contact', function () {
		
		_AddressBook.addContact(this_Contact);

		expect(_AddressBook.getContact(0)).toBe(this_Contact);
	});

	it('should be able to delete a contact', function (){
		
		_AddressBook.addContact(this_Contact);
		_AddressBook.deleteContact(0);

		expect(_AddressBook.getContact(0)).not.toBeDefined();
	});
});

describe('Async Address Book', function (){
	var _AddressBook  = new AddressBook();

	beforeEach(function (done){
		_AddressBook.getInitialContacts(function (){
			done(); //Jasmine method signals that async function done
		});
	});

	it('Should grab initial contacts', function (done){  //pass done and

		_AddressBook.getInitialContacts();
		expect(_AddressBook.initialComplete).toBe(true);
		done();                                          //then call done

	});
});